import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { InvoicesComponent } from './invoices/invoices.component';
import { InvoiceFormComponent } from './invoice-form/invoice-form.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { RouterModule, Routes } from '@angular/router';

const appRoutes: Routes = [

  { path: 'invoices', component: InvoicesComponent },
  { path: 'invoiceForm', component: InvoiceFormComponent }, 
  { path: '', component: InvoicesComponent },
  { path: '**', component: PageNotFoundComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    InvoicesComponent,
    InvoiceFormComponent,
    PageNotFoundComponent,
    
  ],
  imports: [
    BrowserModule,
     FormsModule,
    HttpModule,
     RouterModule.forRoot(appRoutes),
   
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
